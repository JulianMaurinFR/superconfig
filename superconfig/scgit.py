#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Git wrapper using PythonGit
TODO: get modified, get tracked, get staged ( with exception handling )
"""

import os
import git
import click

from .console import line, title, subtitle


def print_git_exception(exception):  
    # Get error attributes
    cmd = exception._cmdline
    stderr = exception.stderr
    code = exception.status
    # Print
    click.echo(title('Git error'))
    click.echo(subtitle('Status code {0}'.format(code)))
    click.echo(subtitle('Command:\n{}'.format(cmd)))
    msg = stderr.replace('  stderr: ', '')
    msg = msg.replace("'", '').strip()
    click.echo(subtitle('Error:\n{}'.format(msg)))

def git_echo(string):
    click.echo('[Git] {}'.format(string))

def clone(repository_url, path):
    try:
        git_echo('Clone from {}'.format(repository_url))
        git.Repo.clone_from(repository_url, path)
    except git.exc.GitCommandError as ex:
        msg = print_git_exception(ex)
        return False
    git_echo('Clone success.')
    return True

def load(location):
    try:
        return git.Repo(location)
    except git.exc.InvalidGitRepositoryError as ex:
        return None

def commit(config):
    staged = len(config.repository.index.diff("HEAD")) > 0
    if not staged:
        return
    try:
        commit_msg = config.repository.git.commit(m='\n'.join(config.events))
    except git.exc.GitCommandError as ex:
        msg = print_git_exception(ex)
        return False
    msg = ''.join(commit_msg.split('\n')[:2])
    git_echo('Commit: {}'.format(msg))
    return True

def stage(config, arg):
    untracked = len(config.repository.untracked_files) > 0
    modified = len(config.repository.index.diff(None)) > 0
    if not untracked and not modified:
        return False
    try:
        config.repository.git.add(arg)
    except Exception as ex:
        msg = print_git_exception(ex)
        return False
    path = os.path.join(config.repository.working_dir, arg)
    git_echo('Staged: {}'.format(path))
    return True

def push(config):
    try:
        config.repository.git.push()
    except Exception as ex:
        msg = print_git_exception(ex)
        return False
    git_echo('Pushed.')
    return True
    