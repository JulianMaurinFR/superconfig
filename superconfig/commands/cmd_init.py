import click
from superconfig.superconfig import pass_repo
from superconfig.console import subtitle

@click.command('init', short_help='Initializes a repo.')
@click.argument('url', required=True, type=click.STRING)
@click.option('-n', '--name', default='superconfig', type=click.STRING)
@click.option('-l', '--location', default='~', type=click.Path(resolve_path=True))
@pass_repo
def cli(repo, url, name, location):
    """TODO: WRITE HELP FOR INIT COMMAND.
    """
    click.echo('Name : {}'.format(name))

    click.echo(subtitle('Initialize configuration'))
    repo.init(name, url)
    click.echo('Config "{}" successfully initialized.'.format(name))
    click.echo()
    click.echo('Now add item to your config and push them !')
    click.echo('Check SuperConfig usage with "TODO call usage"')
