#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
SuperConfig Settings
"""

import os
from pathlib import Path

# HARDCONF
APP_NAME = 'superconfig'
DEFAULT_CONFIG_NAME = APP_NAME
DEFAULT_HOME = os.path.join(
    str(Path.home()),
    APP_NAME
)

# CUSTOM
VAR_HOME = 'SUPERCONFIG_HOME'
