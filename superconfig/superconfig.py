#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
SuperConfig
"""

import os
import sys
import click
from .filesystem import mkdir, copy, write, fsecho
from .scgit import clone, stage, commit, push
from .settings import APP_NAME, DEFAULT_CONFIG_NAME, DEFAULT_HOME, VAR_HOME
from .xml import config_to_xml
from .models import Config, Item
from .console import line, title, subtitle
from superconfig import __version__


CONTEXT_SETTINGS = dict(auto_envvar_prefix='SCONFIG')


####################################
# sconfig config
# sconfig configs
# sconfig activate —confname
# sconfig init —confname —url 
# sconfig add —source —topic —name 
# sconfig status
# sconfig diff 
# sconfig apply —topic
####################################


class SuperConfig(object):
  
    def __init__(self):
        pass

    @property
    def home(self):
        custom_home = os.environ.get(VAR_HOME)
        default_home = DEFAULT_HOME
        home = custom_home or default_home
        if not os.path.isdir(home):
            os.mkdir(home)
        return home

    def quit(self):
        # Quit routine
        exit(1)

    def init(self, name, url):
        """
        TODO: Get valid config name
        """
        # User input
        config_name = name
        repository_url = url
        # Clone repository
        config_path = os.path.join(self.home, config_name)
        if not clone(repository_url, config_path):
            self.quit()
        # Create config obj
        config = Config(config_name, config_path)
        # Add event Initialize
        config.events.append('Initialize SuperConfig')
        # Save
        self.save(config)
        # End
        self.quit()

    def save(self, config):
        """
        Save config
        """
        # Config as XML
        config_xml = config_to_xml(config)
        # Save XML to file
        write(config_xml, config.path)
        # Stage config 
        if stage(config, '.'):
            if commit(config):
                if not push(config):
                    self.quit()

    def load(self, config_name):
        """ Load a config from name """
        # Get config path based on name
        path = os.path.join(self.home, config_name)
        if not os.path.isdir(path):
            raise Exception('Does not exists')
        # Load config
        config = Config(config_name, path)
        if not config.repository:
            raise Exception('Not a valid repository')
        # TODO: Load XML
        return config

    def add(self, config_name, topic, item_name, item_type, item_source):
        # Subtitle
        click.echo(subtitle('Add {} to {}'.format(item_name, topic)))
        # Load config
        config = self.load(config_name)
        # Create item object
        item = Item(item_name, item_type, item_source)
        # Create topic directory
        topic_path = os.path.join(self.home, config_name, topic)
        mkdir(topic_path)
        # Copy item to repository
        if not os.path.isfile(item.source):
            raise Exception('Not a valid file')
        path = os.path.join(topic_path, item_name)
        copy(item_source, path)
        # Add item to config
        config.add(topic, item)
        config.events.append('Add {}'.format(item_name))
        # Save config
        self.save(config)
        self.quit()


pass_repo = click.make_pass_decorator(SuperConfig, ensure=True)
cmd_folder = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                          'commands'))


class SuperConfigCLI(click.MultiCommand):
  
    def list_commands(self, ctx):
        rv = []
        for filename in os.listdir(cmd_folder):
            if filename.endswith('.py') and \
               filename.startswith('cmd_'):
                rv.append(filename[4:-3])
        rv.sort()
        return rv

    def get_command(self, ctx, name):
        try:
            if sys.version_info[0] == 2:
                name = name.encode('ascii', 'replace')
            mod = __import__('superconfig.commands.cmd_' + name,
                             None, None, ['cli'])
        except ImportError:
            return
        return mod.cli


@click.command(cls=SuperConfigCLI, context_settings=CONTEXT_SETTINGS)
@click.version_option(__version__)
@pass_repo
def sconfig(ctx):
    """A simple and powerful Tools for system configuration management based on Git.
    """
    click.echo(title(APP_NAME))
    ctx.obj = SuperConfig()


if __name__ == "__main__" : 
    # sc = SuperConfig()
    # sc.init()
    sc = SuperConfig()
    sc.add('test', 'host', 'hosts', Item.Type.FILE, '/etc/hosts')
