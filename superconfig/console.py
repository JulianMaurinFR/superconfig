#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Console wrapper
"""

LINE_LENGTH = 50

def line():
    line = "="*LINE_LENGTH
    return line

def title(string):
    line_length = int((LINE_LENGTH - (len(string)+2))/2)
    line = '='*line_length
    title = str("{0} {1} {0}").format(line, string, line)
    return title

def subtitle(string, level=1, chip='>'):
    sub = chip*level
    return ' {0} {1}'.format(sub, string)

def question(string, default=None, require=False):
    default = '({}) '.format(default) if default else ''
    question = '{0} : {1}'.format(string, default)
    awnser = None
    while True:
        awnser = input(question)
        if (awnser or not require or default): break
        print('Type a value to continue or quit (CTRL+C).')
    return awnser
