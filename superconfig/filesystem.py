#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Filesystem wrapper
TODO: Handle errors
"""

import os
import shutil


def fsecho(string):
    print('[FS] {}'.format(string))

def write(content, path):
    with open(path, 'w+') as stream:
        stream.write(content)
    fsecho('Create file: {}'.format(path))

def copy(copyfrom, copyto):
    shutil.copy2(copyfrom, copyto)
    fsecho('Copy from "{}" to "{}"'.format(copyfrom, copyto))

def mkdir(path):
    if not os.path.exists(path):
        os.mkdir(path)
        fsecho('Create directory "{}"'.format(path))
