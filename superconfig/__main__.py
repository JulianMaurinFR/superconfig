#! /usr/bin/env python3

import platform
import os
import sys
import superconfig.superconfig as sconfig

def main():
    """Execution begins here."""
    sconfig.sconfig()


if __name__ == "__main__":
    main()
