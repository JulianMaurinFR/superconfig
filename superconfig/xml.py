#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Beautiful soup wrapper
"""

from bs4 import BeautifulSoup, Tag

def node(xml, name, attrs=None):
    attrs = attrs or {}
    return Tag(
        builder=xml.builder, 
        name=name, 
        attrs=attrs
    )

def config_to_xml(config):
    xml = BeautifulSoup(features='xml')
    root_node = node(xml, 'superconfig', {'name': config.name})
    xml.append(root_node)
    # Add topics
    for topic in config.topics:
        topic_node = node(xml, 'topic', {'name': topic})
        # Add items
        for item in config.topics[topic]:
            item_node = node(xml, item.type, {
                'name': item.name,
                'source': item.source
            })
            topic_node.append(item_node)
        root_node.append(topic_node)
    #
    return xml.prettify('utf-8').decode('utf-8')

def config_from_xml(config):
    pass
