#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Models
"""

import os

from .scgit import load

class Item(object):
    class Type:
        FILE = 'file'
        DIRECTORY = 'directory'
        COMMAND = 'command'

    def __init__(self, item_name, item_type, item_source):
        self.name = item_name
        self.type = item_type
        self.source = item_source

class Config(object):

    FILENAME = 'superconfig.xml'

    def __init__(self, name, location):
        self.name = name
        self.repository = load(location)
        self.topics = {}
        self.events = []
    
    def add(self, topic, item):
        if topic not in self.topics:
            self.topics[topic] = []
        self.topics[topic].append(item)

    @property
    def path(self):
        return os.path.join(self.repository.working_dir, self.FILENAME)

