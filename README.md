# SuperConfig  

[![Build Status](https://travis-ci.org/JulianMaurin/SuperConfig.svg?branch=develop)](https://travis-ci.org/JulianMaurin/SuperConfig)

## A tool for system configuration management based on Git and powered by Python.

With few commands you will be able to:

* Save your files, directories or commands outputs.
* Apply a config to a new system for a fast setup.
* Compare your current config with the saved one.
* Track your configuration evolution.
* Revert to an older configuration.

Enjoy the power of Git for your saved files. Share your configurations with public repository or keep them safe on your company repository.

[Documentation](https://julianmaurin.github.io/SuperConfig/devdoc)

