#! /usr/bin/python3

import sys

try:
    from setuptools import setup
except ImportError:
    print("Missing package setuptools. Please install to continue\n(https://pypi.python.org/pypi/setuptools)")
    sys.exit('Exiting now!!')

DESCRIPTION = 'A symple and powerful Tools for system configuration management based on Git'
LONG_DESCRIPTION = 'Please visit https://github.com/JulianMaurin/SuperConfig for docs.'
VERSION = '0.0.1'

with open('requirements.txt') as f:
    REQUIREMENTS = f.read().splitlines()

setup(
    name="SuperConfig",
    version=VERSION,
    author="JulianMaurin",
    author_email="",
    description=(DESCRIPTION),
    license="MIT",
    url="https://github.com/JulianMaurin/SuperConfig",
    packages=['superconfig', 'superconfig.commands'],
    install_requires=REQUIREMENTS,
    long_description=(LONG_DESCRIPTION),
    entry_points={'console_scripts': ['sconfig = superconfig.__main__:main']},
    zip_safe=False,
    keywords=['GIT', 'CLI', 'backup'],
    classifiers=[
        "Development Status :: 1 - Planning",
        "Environment :: Console",
        "License :: OSI Approved :: MIT",
        "Programming Language :: Python :: 3.6",
        "Topic :: Utilities",
    ],
)
