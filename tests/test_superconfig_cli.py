import pytest
import click
from click.testing import CliRunner
from superconfig.superconfig import sconfig


def test_sconfig_run_without_problem():
  runner = CliRunner()
  result = runner.invoke(sconfig)
  assert result.exit_code == 0

def test_superconfig_version():
  runner = CliRunner()
  result = runner.invoke(sconfig, ['--version'])
  assert result.exit_code == 0
  assert result.output == "sconfig, version 0.0.1\n"

