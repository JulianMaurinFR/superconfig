import pytest
import click
from click.testing import CliRunner
from superconfig.superconfig import sconfig
from superconfig.settings import DEFAULT_HOME

def test_command_init_without_parameters():
  runner = CliRunner()
  result = runner.invoke(sconfig, ['init'])
  assert "Error: Missing argument \"url\"." in result.output
  assert result.exit_code == 2


def test_command_init_with_fake_url_parameter():
  runner = CliRunner()
  result = runner.invoke(sconfig, ['init', 'url', 'fakeurl'])
  assert result.exit_code == 1
  assert "repository fakeurl does not exist" in result.output


def test_command_init_with_url_parameter():
  runner = CliRunner()
  result = runner.invoke(sconfig, ['init', 'url', 'https://github.com/nicolasmsg/test_superconfig.git'])
  assert result.exit_code == 1
  assert "repository fakeurl does not exist" in result.output