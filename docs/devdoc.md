# SuperConfig detailed specification


## Commads

### __[A-000]__ `config`

command: 

    sconfig config

output:

    $> [active-config-name]


### __[B-000]__ `configs`

command: 

    sconfig configs

output:

Display a list of config sorted alphabetically
The active config is prefixed by the char "*"

    $>   ...
    $> * [active-config-name]
    $>   [config-name]
    $>   ...

### __[C-000]__ `activate`

command: 

    sconfig activate [config-name]

output:

    $> [config-name] activated


### __[D-000]__ `init`

command: 

    sconfig init [config-name] [config-url]

output:

    # Sucess
    $> [config-name] initialized

    # Already exists error
    $> [config-name] already exists

    # Git error
    $> Repository clone failure
    $> [git-command-output]

### __[E-000]__ `add`

command: 

    sconfig add [topic-name] [item-path] 
    # or
    sconfig add [topic-name]:[item-name] [item-path] 

output:

    $> "[item-name]" added to [config-name]

    # File not found error
    $> No such file or directory

    # Already exists error
    $> [item-name] already exists

    # Invalid format
    $> [item-name] is not a valid item name


### __[F-000]__ `addcommand`

command: 

    sconfig addcommand [topic-name] "[item-command]"
    # or
    sconfig addcommand [topic-name]:[item-name] "[item-command]" 

output:

    $> "[item-name]" added to [config-name]

    # Invalid command
    $> Invalid command

    # Already exists error
    $> [item-name] already exists

    # Invalid format
    $> [item-name] is not a valid item name

### __[G-000]__ `remove`

command: 

    sconfig remove [item-name]

output:

    $> [item-name] removed
    
    # File not found error
    $> No such file or directory

    # Remove error
    $> Unable to remove: 
    $>     [item-path]
    $>     ...

### __[H-000]__ `cease`

command: 

    sconfig cease

output:

    $> cease will remove the following files:
    $>     [item-path]
    $>     [item-path]
    $> Are your sure to continue ? [y/N]:

    # Remove error
    $> Unable to remove: 
    $>     [item-path]
    $>     ...


## Models

active-config-name

    /^[a-z0-9_-]{3,50}$/

config-name

    /^[a-z0-9_-]{3,50}$/

config-url

    /^((http[s]?):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?\.git$/

topic-name
    
    /^[a-z0-9_-]{3,50}$/

    default: "general" 

item-name

    /^[a-z0-9_-]{3,50}$/

item-path

    /^.*$/

item-command

    /^.*$/


## Exception

* File not found error
* Already exists error
* Repository clone failure
* Invalid format
* Invalid command
* Remove error




_________________________________________


### [-000] status

command: 

    sconfig status

output:

    $> Repository:
    $>  [repository-item-added-count] item added 
    $>  [repository-item-updated-count] item updated
    $>  [repository-item-removed-count] item removed 
    $> Use 'sconfig push' to save your current configuration
    $>
    $> System:
    $>  [system-item-edited-count] item edited
    $>  [system-item-removed-count] item removed 
    $> Use 'sconfig apply' to revert your system configuration
    $> Use 'sconfig update' to update [active-configuration-name] from system


### __[-000]__ `diff

command: 

    sconfig diff

output:

    $> [system-item-status] [file]
    $> [missing]     missing [file]
    $> Use 'sconfig apply' to synchronise your system with the active configuration

### __[-000]__ `apply

command: 

    sconfig apply

output:

    $> [active-config-name]

    # 
    $> 
